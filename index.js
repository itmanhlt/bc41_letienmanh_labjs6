// lab 1
var btnBai1 = document.querySelector("#btnBai1");

btnBai1.addEventListener("click", function () {
  var sum = 0;
  for (var i = 1; i < 1000; i++) {
    sum += i;
    if (sum > 10000)
      return (document.querySelector(
        "#kqBai1"
      ).innerHTML = `Số nguyên dương nhỏ nhất: ${i}`);
  }
});
// lab 2
var btnBai2 = document.querySelector("#btnBai2");

btnBai2.addEventListener("click", function () {
  var x = document.querySelector("#nhapX").value * 1;
  var n = document.querySelector("#nhapN").value * 1;
  var sum = 0;
  for (var i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }
  document.querySelector("#kqBai2").innerHTML = `Tổng: ${sum}`;
});
// lab3
var btnBai3 = document.querySelector("#btnBai3");

btnBai3.addEventListener("click", function () {
  var n = document.querySelector("#nhapSoN").value * 1;
  var result = 1;
  for (var i = 1; i <= n; i++) {
    result *= i;
  }
  document.querySelector("#kqBai3").innerHTML = `Giai thừa: ${result}`;
});
//lab4
var btnBai4 = document.querySelector("#btnBai4");

btnBai4.addEventListener("click", function () {
  var ketQua = document.querySelector("#kqBai4");
  for (var i = 1; i <= 10; i++) {
    var newDiv = document.createElement("div");
    newDiv.innerHTML = "Div lẻ";
    newDiv.style.backgroundColor = "#0D6EFD";
    if (i % 2 != 0) {
      ketQua.appendChild(newDiv);
    } else {
      ketQua.appendChild(newDiv);
      newDiv.innerHTML = "Div chẵn";
      newDiv.style.backgroundColor = "#DC3545";
    }
  }
});
//lab5
var btnBai5 = document.querySelector("#btnBai5");

function isNguyenTo(number) {
  if (number < 2) {
    return false;
  }
  for (var i = 2; i <= Math.sqrt(number); i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}

btnBai5.addEventListener("click", function () {
  var number = document.getElementById("txt-number").value * 1;
  var result = "";
  for (var i = 0; i < number; i++) {
    if (isNguyenTo(i)) {
      result += i + " ";
    }
  }
  document.querySelector("#kqBai5").innerHTML = `${result} `;
});
